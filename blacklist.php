<?php
declare(strict_types=1);

require_once 'security.php';
require_once 'connection.php';
require_once 'session.php';

$style_nonce = generate_nonce();

// if (false):
// 	header(implode(': ', [
// 		'Content-Security-Policy',
// 		implode('; ',
// 			array_map(function ($arr) { return implode(' ', $arr); }, [
// 				['script-src', "'none'"],
// 				['style-src', "'nonce-$style_nonce'"],
// 				['default-src', "'self'"]
// 			])
// 		)
// 	]), false);
// endif;

$_page_title = 'Blacklist';

include_once 'header.inc';
?>

<div class="container">

<?php
	if ($_SERVER['REQUEST_METHOD'] === 'POST'):
		$bid   = $_POST['i'];
		$bname = $_POST['n'];
		$bmail = $_POST['e'];

?>

<?php
		$req = "UPDATE `users` SET `enabled` = ((`enabled` + 1) % 2) "
			. "WHERE `users`.`id` = ? "
			. "AND `users`.`name` = ? "
			. "AND `users`.`email` = ? "
			. "LIMIT 1";
		$stmt = $conn2->prepare($req);

		if ($stmt === FALSE) {
			die(mysqli_error($conn2));
		}

		$stmt->bind_param("iss", $bid, $bname, $bmail);

		if (! $stmt->execute()) {
			die($stmt->error());
		}

		$affected_rows = $stmt->affected_rows;

		$stmt->close();

		$req = "SELECT `enabled` FROM `users` "
			. "WHERE `users`.`id` = ? "
			. "AND `users`.`name` = ? "
			. "AND `users`.`email` = ? "
			. "LIMIT 1";
		$stmt = $conn2->prepare($req);
		if ($stmt === FALSE) {
			die(mysqli_error($conn2));
		}
		$stmt->bind_param("iss", $bid, $bname, $bmail);
		if (! $stmt->execute()) {
			die($stmt->error());
		}
		$res = $stmt->get_result();
		if ($res === FALSE) {
			die($stmt->error);
		}
		$row = $res->fetch_row();
		$stmt->free_result();
		$res = null;
		$stmt->close();

		$blocked_text = $row[0] ? "d&eacute;" : "";
?>


	<h1>Cet utilisateur a &eacute;t&eacute; 
		<?= $blocked_text ?>block&eacute;</h1>
	<dl class="row">
		<dt class="col-md-2">ID</dt>
			<dd class="col-md-4"><?= $bid ?></dd>
		<div class="w-100"></div>
		<dt class="col-md-2">Name</dt>
			<dd class="col-md-4"><?= $bname ?></dd>
		<div class="w-100"></div>
		<dt class="col-md-2">Mail</dt>
			<dd class="col-md-4"><code><?= $bmail ?></code></dd>
	</dl>

<?php else: ?>

	<h1>Spam détecté</h1>
	<table class="table table-striped">
		<tbody>
			<?php
				$req = "SELECT COUNT(m.id) AS nbr, m.id, m.name, m.email, m.enabled "
					. "FROM mails s "
					. "JOIN users m ON m.id = s.sender_id "
					. "WHERE s.is_spam = 'SPAM' "
					. "GROUP BY m.email";
				$rslt = mysqli_query($conn2, $req) or die(mysqli_error($conn2));
				while ($lan = MYSQLi_FETCH_array($rslt)): ?>
			<tr>
				<td><?= $lan['name'] ?></td>
				<td><?= $lan['email'] ?></td>
				<td><?= $lan['nbr'] ?></td>
				<td><?= $lan['enabled'] ? 'Oui' : 'Non' ?></td>
				<td class="text-right">
					<form method="post">
						<?php foreach (['id', 'name', 'email'] as $key): ?>
						<input type="hidden" name="<?= $key[0] ?>" value="<?= $lan[$key] ?>" />
						<?php endforeach ?>
						<input
							type="submit"
							class="btn btn-sm <?= $lan['enabled'] ? 'btn-danger' : 'btn-secondary' ?>"
								value="<?= $lan['enabled'] ? 'Block' : 'Unblock' ?>"
							name="s"
						/>
					</form>
				</td>
			</tr>
			<?php endwhile ?>
		</tbody>
		<thead>
			<tr>
				<th>Nom</th>
				<th>Mail</th>
				<th>Nombre de Spam Envoyé</th>
				<th>Compte actif</th>
				<th>Bloquer</th>
			</tr>
		</thead>
	</table>

<?php endif ?>

</div> <!-- container -->
</body>
</html>
