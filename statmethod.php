<?php
declare(strict_types=1);
require_once 'security.php';
require_once 'connection.php';

function get_method(int $method_id): array
{
	global $conn2;

	$query = 'SELECT id, name, accuracy, `precision`, recall, f1, roc_auc FROM spam_methods WHERE id = ?';

	$stmt = $conn2->stmt_init();
	$stmt->prepare($query);
	$stmt->bind_param("i", $method_id);
	$stmt->execute();

	$r = $stmt->get_result();

	if ($r->num_rows !== 1) {
		die("Could not get method $method_id!");
	}

	$m = $r->fetch_array(MYSQLI_ASSOC);

	$r->free();

	return $m;
}

function get_mails_detected_with_method(int $method_id): array
{
	global $conn2;

	$query = <<<'EOQ'
		SELECT
			m.id,
			m.date,
			m.subject,
			m.sender_id,
			u.id as sender_id,
			u.name as sender_name,
			u.email as sender_email
		FROM
			spam_methods meth
		JOIN `spam_results` r ON
			meth.id = r.`method_id`
		JOIN `mails` m ON
			m.id = r.`mail_id`
		JOIN `users` u ON
			u.id = m.`sender_id`
		WHERE
			meth.id = ? AND r.is_spam = 1
EOQ;

	$stmt = $conn2->stmt_init();
	$stmt->prepare($query);
	$stmt->bind_param("i", $method_id);
	$stmt->execute();

	$r = $stmt->get_result();

	$spams = $r->fetch_all(MYSQLI_ASSOC);

	$r->free();

	return $spams;
}

function get_detection_rate(int $method_id): array
{
	global $conn2;

	$query = <<<'EOQ'
		SELECT
		    `is_spam`,
		    COUNT(`mail_id`) `count`
		FROM
		    `spam_results`
		WHERE
		    `method_id` = ?
		GROUP BY
		    `is_spam`
EOQ;

	$stmt = $conn2->stmt_init();
	$stmt->prepare($query);
	$stmt->bind_param("i", $method_id);
	$stmt->execute();

	$r = $stmt->get_result();

	$rate = array();

	while ($row = $r->fetch_assoc()) {
		$key = $row['is_spam'] ? 'Spam' : 'Ham';
		$rate[$key] = $row['count'];
	}

	$r->free();

	return $rate;
}

function gen_metric_name(string $key): string
{
	return ucwords(strtr(str_replace('roc_auc', 'ROC_AUC', $key), '_', ' ')) . ($key === 'name' ? '' : ' score');
}

function format_percentage(string $key, $value): string
{
	return $key === 'name' ? $value : sprintf('%2.4f%%', 100 * (double) $value);
}

$mid = (int) $_GET['method'];

$method = get_method($mid);
$spams = get_mails_detected_with_method($mid);
$rate = get_detection_rate($mid);

//header('Content-Type: application/json');
//print(json_encode($method));
//print(json_encode($spams));
//print(json_encode($rate));
//exit();

$_page_title = 'Statistics for method ' . $method['name'];
include_once 'header.inc';
?>
	<div class="container">

	<h1 clss="mx-3">Statistics for method “<?= $method['name'] ?>”</h1>

	<div class="row">
		<div class="col-md-5 m-auto">
			<figure class="figure">
				<canvas id="pie-chart" class="figure-img img-fluid rounded bg-secondary p-3"></canvas>
				<figcaption class="figure-caption">Detection rate for <?= $method['name'] ?>.</figcaption>
			</figure>
		</div>
		<div class="col-md-7">
			<h3>Processed emails</h3>
			<dl class="row">
				<dt class="col-md-6">Total number of processed emails</dt>
				<dd class="col-md-6"><?= $rate['Spam'] + $rate['Ham'] ?></dd>

				<dt class="col-md-6">Emails detected as Spam</dt>
				<dd class="col-md-6"><?= $rate['Spam'] ?></dd>

				<dt class="col-md-6">Emails detected as Ham</dt>
				<dd class="col-md-6"><?= $rate['Ham'] ?></dd>
			</dl>

			<h3>Model metrics</h3>
			<dl class="row">
			<?php foreach (['name', 'accuracy', 'precision', 'recall', 'f1', 'roc_auc'] as $metric): ?>
				<dt class="col-md-3"><?= gen_metric_name($metric); ?></dt>
				<dd class="col-md-3"><?= format_percentage($metric, $method[$metric]); ?></dd>
			<?php endforeach ?>
			</dl>
		</div>
	</div>

	<h1 class="mt-3">Emails detected as spam</h1>
	<p>Below is a list of all emails that have been detected as spam by this method.<br>
		You can click on the subject or the date of the email to open it and read it,
		or you can click the block button to add the sender of the email to the block list.</p>

	<table class="table">
	<thead>
		<tr>
		<th scope="col">Sender</th>
		<th scope="col">Subject</th>
		<th scope="col">Date</th>
		<th scope="col">Block user</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($spams as $message): ?>
		<tr>
		<td><?= $message['sender_name'] ?> <code>&lt;<?= $message['sender_email'] ?>&gt;</code></td>
		<td><a class="text-reset" href="/message.php?message=<?= $message['id'] ?>"><?= $message['subject'] ?></a></td>
		<td><a class="text-reset" href="/message.php?message=<?= $message['id'] ?>">
			<?= (new DateTime($message['date']))->format(DateTimeInterface::RFC2822) ?></a></td>
		<td>	
			<form method="post" action="/blacklist.php">
			<?php foreach (['id', 'name', 'email'] as $key): ?>
				<input
					type="hidden"
					name="<?= $key[0] ?>"
					value="<?= $message["sender_$key"] ?>">
			<?php endforeach ?>
				<input
					type="submit"
					class="btn btn-sm btn-danger"
					value="Block"
					name="s">
			</form>
		</td>
		</tr>
		<?php endforeach; ?>
	</tbody>
	</table>

	</div><!-- container -->

	<script src="js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
	<script src="js/chart.min.js"></script>
	<script src="chart-colors/chartjs-plugin-colorschemes.min.js"></script>

	<script>
		Chart.defaults.color = 'white';
		Chart.defaults.plugins.colorschemes.scheme = 'tableau.Tableau20';

		var data = {
			labels: <?= json_encode(array_keys($rate)); ?>,
			datasets: [
				{
					data: <?= json_encode(array_values($rate)); ?>
				}
			]
		};
		var chart = new Chart('pie-chart', {
			type: 'pie',
			data: data,
			options: {
				plugins: {
					title: {
						text: 'Detection rate'
					}
				}
			}
		});
	</script>
</body>
