<!DOCTYPE html>
<html lang="fr">
<head>
<meta charset="utf-8">
<title><?= $_page_title ?></title>
<style type="text/css">
	.mx-auto {
		margin-left: auto;
		margin-right: auto;
	}
	.my-3 {
		margin-top: 3rem;
		margin-bottom: 3rem;
	}
	.w-60 {
		width: 60%;
	}
</style>
<link
	rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css"
	integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu"
	crossorigin="anonymous"
	<?php 
		if (isset($style_nonce)) {
			echo 'nonce="' . $style_nonce . '"';
		}
	?>>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap-theme.min.css" integrity="sha384-6pzBo3FDv/PJ8r2KRkGHifhEocL+1X2rVCTTkUfGk7/0pbek5mMa1upzvWbrUbOZ" crossorigin="anonymous">
</head>
<body>

<?php
	function navbar_active(string $page): string
	{
		$endpoint = pathinfo($_SERVER["REQUEST_URI"], PATHINFO_FILENAME);
		return $endpoint . ".php" === $page ? ' class="active"' : '';
	}

	$pages = null;
	if (is_logged_in()) {
		$pages = array(
			"statindex2.php" => "Home",
			"blacklist.php"  => "Black list",
		);
	} else {
		$pages = array(
			"index.php"       => "Log in",
			"signup.php"      => "Sign up",
		);
	}
?>

<nav class="navbar navbar-default">
	<div class="container-fluid">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="#">SpamMailer &ndash; Dashboard</a>
		</div>

		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			<ul class="nav navbar-nav">
				<?php foreach ($pages as $page => $label): ?>
					<li <?= navbar_active($page) ?>>
						<a class="nav-link" href="/<?= $page ?>"><?= $label ?></a>
					</li>
				<?php endforeach ?>
			</ul>
			<ul class="nav navbar-nav navbar-right">
				<?php if (is_logged_in()): ?>
					<li><a href="/logout.php">Log out (<?= logged_in_user() ?>)</a></li>
				<?php endif ?>
			</ul>
		</div><!-- /.navbar-collapse -->
	</div><!-- /.container-fluid -->
</nav>
