<?php
declare(strict_types=1);
require_once 'security.php';
require_once 'connection.php';

class DataQuerier
{
	private $connection;
	private $fields;

	public function __construct(mysqli &$connection)
	{
		$this->connection =& $connection;
	}

	public function query(string $query): array
	{
		$data = array();

		$result = $this->connection->query($query);

		if ($result === false) {
			die($this->connection->error);
		}

		$this->fields = array_column($result->fetch_fields(), 'name');

		while ($row = $result->fetch_row()) {
			//$data[(string) $row[0]] = (int) $row[1];
			$data[] = $row;
		}

		$result->close();
		unset($result);

		return $data;
	}

	public function getFields(): array
	{
		return $this->fields;
	}
}

function get_spams_by_year(DataQuerier &$querier): array
{
	$format = "SELECT EXTRACT(YEAR FROM date) year, COUNT(*) count FROM mails WHERE is_spam = '%s' GROUP BY year";

	$q_spams = sprintf($format, 'SPAM');
	$q_hams = sprintf($format, 'HAM');

	return array(
		'spams' => $querier->query($q_spams),
		'hams' => $querier->query($q_hams)
	);
}

function get_spams_by_method(DataQuerier &$querier): array
{
	$format = '
		SELECT
		    spam_methods.name,
		    COUNT(mails.id) count
		FROM
		    mails
		JOIN spam_results ON mails.id = spam_results.mail_id
		JOIN spam_methods ON spam_results.method_id = spam_methods.id
		WHERE
		    spam_results.is_spam = %d
		GROUP BY
		    spam_methods.id';

	$q_spams = sprintf($format, 1);
	$q_hams = sprintf($format, 0);

	return array(
		'spams' => $querier->query($q_spams),
		'hams' => $querier->query($q_hams)
	);
}

function get_methods(DataQuerier &$querier): array
{
	//return $querier->query('SELECT id, name, accuracy, `precision`, recall, f1, roc_auc FROM spam_methods');
	return $querier->query('SELECT id, name, accuracy FROM spam_methods');
}

$querier = new DataQuerier($conn2);

$by_year = get_spams_by_year($querier);
$by_method = get_spams_by_method($querier);
$methods = get_methods($querier);
$method_fields = $querier->getFields();

$year_labels = array_unique(array_column(array_merge($by_year['spams'], $by_year['hams']), 0));
sort($year_labels, SORT_NUMERIC);
$year_labels = array_unique($year_labels);

//echo "Querier fields:\n";
//var_dump($querier->getFields());
//echo "\n";

unset($querier);

//echo "By year: " . json_encode($by_year, JSON_PRETTY_PRINT) . "\n";
//echo "By method: " . json_encode($by_method, JSON_PRETTY_PRINT) . "\n";
