<?php
	http_response_code(401);
	$_page_title = "No!";
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title><?= $_page_title ?></title>
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css"
		integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
</head>
<body>
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
		<a class="navbar-brand" href="/">SpamMailer Dashboard</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expand="false" aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span>
		</button>

		<div id="navbarSupportedContent" class="collapse navbar-collapse">
		<ul class="navbar-nav mr-auto">

		<?php foreach (array("index.php" => "Log in", "signup.php" => "Sign up") as $page => $label): ?>
			<li class="nav-item<?= "$endpoint.php" == $page ? ' active' : '' ?>">
				<a class="nav-link" href="/<?= $page ?>"><?= $label ?></a>
			</li>
		<?php endforeach ?>

		</ul>
		</div>
	</nav>
	<div class="container">
		<h1>No</h1>
		<p>You can't sign up.</p>
		<p>Go <a href="/index.php">back</a> from whence you came. Begone!</p>
		<p>You will be redirected in <span id="counter"></span> seconds.</p>
	</div>
	<script>
		function updateCounter(element) {
			s = parseInt(counter.textContent);
			counter.textContent = s - 1;
		}
		window.addEventListener("load", function(event) {
			var counter = document.getElementById("counter");
			var seconds = 5;
			counter.textContent = seconds;
			setTimeout(history.back.bind(history), seconds * 1000 - 1);
			setInterval(updateCounter.bind(counter), 1000);
		});
	</script>
</body>
</html>