<?php
session_start();

function is_logged_in(): bool
{
	return isset($_SESSION['username']);
}

function logged_in_user(): string
{
	return is_logged_in() ? $_SESSION['username'] : '';
}

function generate_nonce(): string
{
	return base64_encode(random_bytes(16));
}
