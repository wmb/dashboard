<?php 

require_once 'session.php';

if (!is_logged_in()) {
	header("Location: /index.php");
}
