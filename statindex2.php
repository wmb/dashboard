<?php
declare(strict_types=1);
require_once("security.php");
require_once("stat3.php");

$_page_title = 'General statistics';
include_once 'header.inc'
?>

<div class="container">
	<h1>General statistics</h1>

	<h2>Spam detection methods</h2>
	<p>Here's a list of the methods that we use to detect spam. Click on a method to see more information about it.<br>
		The method's accuracy is shown to the right.</p>
	<div class="list-group w-60 mx-auto my-3">
<?php foreach ($methods as $method): ?>
		<a class="list-group-item list-group-item-action d-flex justify-content-between align-items-center"
			href="/statmethod.php?method=<?= $method[0] ?>">
			<?= $method[1] ?>
			<span class="badge bg-primary rounded-pill"><?php printf('%2.3f%%', 100*$method[2]); ?></span>
		</a>
<?php endforeach; ?>
	</div>

	<div class="row">
		<div class="col-md-6">
			<canvas id="myChart3"></canvas>
		</div>
		<div class="col-md-6">
			<canvas id="myChart4"></canvas>
		</div>
	</div>
</div>

<script src="js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
<script src="js/chart.min.js"></script>
<script src="chart-colors/chartjs-plugin-colorschemes.min.js"></script>

<script>
	Chart.defaults.plugins.title.display = true;
	Chart.defaults.plugins.colorschemes.scheme = 'tableau.Tableau20';

	var chart3 = (function() {
		var labels = <?= json_encode(array_column($by_method['spams'], 0)) ?>;
		var data = {
			labels: labels,
			datasets: [
				{
					label: 'Spam',
					data: <?= json_encode(array_column($by_method['spams'], 1, 0)) ?>
				},
				{
					label: 'Ham',
					data: <?= json_encode(array_column($by_method['hams'], 1, 0)) ?>
				},
			]
		};
		var chart = new Chart('myChart3', {
			type: 'bar',
			data: data,
			options: {
				plugins: {
					title: {
						display: true,
						text: 'By method'
					}
				},
				scales: {
					x: {
						stacked: true
					},
					y: {
						stacked: true,
						beginAtZero: true,
						ticks: {
							callback: v => v % 1 === 0 ? v : null
						}
					}
				}
			}
		});
	})();

	var methods = <?= json_encode($methods) ?>;

	var chart4 = (function() {

		var labels = <?= json_encode($year_labels) ?>;
		var data = {
			labels: labels,
			datasets: [
				{
					label: 'Spam',
					data: <?= json_encode(array_column($by_year['spams'], 1, 0)) ?>
				},
				{
					label: 'Ham',
					data: <?= json_encode(array_column($by_year['hams'], 1, 0)) ?>
				},
			]
		};
		var chart = new Chart('myChart4', {
			type: 'bar',
			data: data,
			options: {
				plugins: {
					title: {
						display: true,
						text: 'By year'
					}
				},
				scales: {
					x: {
						stacked: true
					},
					y: {
						stacked: true,
						beginAtZero: true,
						ticks: {
							callback: v => v % 1 === 0 ? v : null
						}
					}
				}
			}
		});
	})();

	//var chart1 = new Chart('myChart', {
		//type: 'line',
		//data: <!--?= json_encode($by_year_data) ?-->,
		//options: {
			//plugins: {
				//title: {
					//text: "Number of spams detected by year"
				//}
			//},
			//scales: {
				//y: {
					//beginAtZero: true
				//}
			//}
		//}
	//});
	//var chart2 = new Chart('myChart2', {
		//type: 'bar',
		//data: <!--?= json_encode($by_method_data) ?-->,
		//options: {
			//plugins: {
				//title: {
					//text: "Number of spams detected by each method"
				//}
			//},
			//scales: {
				//y: {
					//beginAtZero: true
				//}
			//}
		//}
	//});
</script>

</body>
</html>
