<?php
declare(strict_types=1);
require_once 'session.php';

$invalid_login = false;
$success_route = '/statindex2.php';

if (is_logged_in()) {
	header("Location: $success_route");
	exit();
}

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
	require_once 'connection.php';

	$u = $_POST['username'];
	$p = $_POST['password'];

	$query = 'SELECT `username`, `password` FROM `admins` WHERE `username` = ? LIMIT 1';

	$stmt = $conn2->stmt_init();
	$stmt->prepare($query);
	$stmt->bind_param("s", $u);
	$stmt->execute();

	$r = $stmt->get_result();

	if ($r->num_rows == 1) {
		$row = $r->fetch_row();
		if (password_verify($p, $row[1])) {
			$_SESSION['username'] = $row[0];
			header("Location: $success_route");
			exit();
		}
	}

	$invalid_login = true;
}
$_page_title = 'Authentication';
$endpoint = pathinfo($_SERVER["REQUEST_URI"], PATHINFO_FILENAME);
include_once "header.inc";
?>
	<div class="container">

		<?php if ($invalid_login): ?>

		<div class="container">
			<div class="alert alert-danger" role="alert">
				Wrong username or password!
			</div>
		</div>

		<?php endif ?>

		<h1>Log in as administrator</h1>

		<div class="container, mt-3 mb-3">
			<form method="post" class="form" enctype="multipart/form-data">
				<div class="form-group">
					<label for="username">Username</label>
					<input type="text" id="username" name="username" class="form-control">
				</div>
				<div class="form-group">
					<label for="password">Password</label>
					<input type="password" id="password" name="password" class="form-control">
				</div>
				<div class="form-group">
					<button type="submit" class="btn btn-primary btn-md">Login</button>
					<button type="reset" class="btn btn-secondary btn-md">Reset</button>
				</div>
			</form>
		</div>

	</div>

</body>
</html>
